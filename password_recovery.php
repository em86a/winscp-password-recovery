<?php
/**
 * WinSCP DecryptPassword port to PHP
 */

//Replace string with the value taken from registry. Don't know where to take? Don't use this.
$cipher = 'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX';

define('PWALG_SIMPLE', 1);
define('PWALG_SIMPLE_MAGIC', 0x5C);
define('PWALG_SIMPLE_STRING', '0123456789ABCDEF');
define('PWALG_SIMPLE_MAXLEN', 50);
define('PWALG_SIMPLE_FLAG', 0xFF);

define('PWALG_SIMPLE_INTERNAL', 0x00);
define('PWALG_SIMPLE_EXTERNAL', 0x01);

echo DecryptPassword($cipher);

function DecryptPassword($Password, $UnicodeKey=''){
    $Key = $UnicodeKey;
    $Result = '';


//    var_dump($Password);

    $Flag = SimpleDecryptNextChar($Password);

//    var_dump($Flag);die;

    if ($Flag == PWALG_SIMPLE_FLAG){
//        var_dump('simple');
        /* Dummy */ SimpleDecryptNextChar($Password);
        $Length = SimpleDecryptNextChar($Password);
    }else{
        $Length = $Flag;
    }

    $Password = substr($Password, SimpleDecryptNextChar($Password) *2 + 2);

//    var_dump($Length);
//    var_dump($Password);

//    $Length = strlen($Password) / 2;
    for ($Index = 0; $Index < $Length; $Index++){
        $r = SimpleDecryptNextChar($Password);
        if ($r == 0){
            break;
        }
//        var_dump(chr($r));
        $Result .= chr($r);
    }

    if ($Flag == PWALG_SIMPLE_FLAG){
        if (substr($Result, 0, strlen($Key)) != $Key){
            $Result = '';
        }else{
            $Result = substr($Result, strlen($Key));
        }
    }

    return $Result;
}

/**
 * @param String $str
 * @return int
 */
function SimpleDecryptNextChar(&$str){
    if (strlen($str)>0){
        $result = (strpos(PWALG_SIMPLE_STRING, $str[0]) * 16 + strpos(PWALG_SIMPLE_STRING, $str[1])) ^ PWALG_SIMPLE_MAGIC;
        $str = substr($str, 2);
        return $result;
    }else{
        return 0x00;
    }
}


/*
unsigned char SimpleDecryptNextChar(RawByteString &Str)
    {
  if (Str.Length() > 0)
  {
    unsigned char Result = (unsigned char)
      ~((((PWALG_SIMPLE_STRING.Pos(Str.c_str()[0])-1) << 4) +
          ((PWALG_SIMPLE_STRING.Pos(Str.c_str()[1])-1) << 0)) ^ PWALG_SIMPLE_MAGIC);
    Str.Delete(1, 2);
    return Result;
  }
  else return 0x00;
}

UnicodeString DecryptPassword(RawByteString Password, UnicodeString UnicodeKey, Integer )
{
  UTF8String Key = UnicodeKey;
  UTF8String Result("");
  Integer Index;
  unsigned char Length, Flag;

  Flag = SimpleDecryptNextChar(Password);
  if (Flag == PWALG_SIMPLE_FLAG)
  {
      / * Dummy = * / SimpleDecryptNextChar(Password);
      Length = SimpleDecryptNextChar(Password);
  }
  else Length = Flag;
  Password.Delete(1, ((Integer)SimpleDecryptNextChar(Password))*2);
  for (Index = 0; Index < Length; Index++)
    Result += (char)SimpleDecryptNextChar(Password);
  if (Flag == PWALG_SIMPLE_FLAG)
  {
      if (Result.SubString(1, Key.Length()) != Key) Result = "";
      else Result.Delete(1, Key.Length());
  }
  return UnicodeString(Result);
}

*/